async function day1A() {
  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    let previous = 0;
    let count = -1;
    for (const line of lines) {
      const current = parseInt(line);
      if (current > previous) {
        count++;
      }
      previous = current;
    }
    return count;
  }

  const sampleInput = `
199
200
208
210
200
207
240
269
260
263
`;

  const actual = process(sampleInput);

  if (actual !== 7) {
    throw "fails";
  }
  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day1A();
