async function day1B(): Promise<void> {
  function createSlices(lines: number[]): number[] {
    const slices: number[] = [];
    for (let index = 0; index < lines.length - 2; index++) {
      slices.push(lines[index] + lines[index + 1] + lines[index + 2]);
    }
    return slices;
  }
  function countIncreasing(values: number[]): number {
    let previous = 0;
    let count = -1;
    for (const value of values) {
      if (value > previous) {
        count++;
      }
      previous = value;
    }
    return count;
  }
  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    const values = lines.map((s) => parseInt(s));
    const slices = createSlices(values);
    return countIncreasing(slices);
  }

  const sampleInput = `
199
200
208
210
200
207
240
269
260
263
`;

  const actual = process(sampleInput);

  if (actual !== 5) {
    throw "fails";
  }
  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day1B();
