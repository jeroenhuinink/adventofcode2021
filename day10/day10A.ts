async function day10A() {
  const errorValues: Record<string, number> = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
  };

  function parseLine(line: string, expecting: string[]): number {
    const head = line.substr(0, 1);
    if (!head) {
      return 0;
    }
    const tail = line.substr(1);
    switch (head) {
      case "(":
        return parseLine(tail, [")", ...expecting]);
      case "[":
        return parseLine(tail, ["]", ...expecting]);
      case "{":
        return parseLine(tail, ["}", ...expecting]);
      case "<":
        return parseLine(tail, [">", ...expecting]);
      default:
        if (head !== expecting[0]) {
          // console.error(
          //   `Expected ${expecting[0]}, but found ${head} instead.`
          // );
          return errorValues[head] ?? -1;
        }
        return parseLine(tail, expecting.slice(1));
    }
  }

  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    return lines.reduce((total, line) => parseLine(line, []) + total, 0);
  }

  const sampleInput = `[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]`;

  const actual = process(sampleInput);

  const expected = 26397;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }
  console.log("test ok");

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day10A();
