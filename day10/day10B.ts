async function day10B() {
  const errorValues: Record<string, number> = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
  };

  function parseLine(line: string, expecting: string[]): number {
    const head = line.substr(0, 1);
    if (!head) {
      return expecting
        .map((e) => errorValues[e])
        .reduce((total, v) => total * 5 + v, 0);
    }
    const tail = line.substr(1);
    switch (head) {
      case "(":
        return parseLine(tail, [")", ...expecting]);
      case "[":
        return parseLine(tail, ["]", ...expecting]);
      case "{":
        return parseLine(tail, ["}", ...expecting]);
      case "<":
        return parseLine(tail, [">", ...expecting]);
      default:
        if (head !== expecting[0]) {
          return -1;
        }
        return parseLine(tail, expecting.slice(1));
    }
  }

  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    const values = lines
      .map((line) => parseLine(line, []))
      .filter((v) => v > 0)
      .sort((a, b) => (a < b ? -1 : 1));

    return values[Math.floor(values.length / 2)];
  }

  const sampleInput = `[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]`;

  const actual = process(sampleInput);

  const expected = 288957;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }
  console.log("test ok");

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day10B();
