async function day11A() {
  function process(input: string): number {
    let flashCount = 0;

    function neighbours(x: number, y: number, map: number[][]): number[][] {
      const maxY = map.length - 1;
      const maxX = map[0].length - 1;
      const neighbours: number[][] = [];
      for (let j = y - 1; j <= y + 1; j++) {
        if (j >= 0 && j <= maxY) {
          for (let i = x - 1; i <= x + 1; i++) {
            if (i >= 0 && i <= maxX) {
              neighbours.push([j, i]);
            }
          }
        }
      }

      return neighbours;
    }

    function increase(octopusses: number[][]): void {
      for (let y = 0; y < octopusses.length; y++) {
        for (let x = 0; x < octopusses[y].length; x++) {
          octopusses[y][x] += 1;
        }
      }
    }

    function flash(x: number, y: number, octopusses: number[][]): number {
      let flashCount = 0;
      const n = neighbours(x, y, octopusses);

      octopusses[y][x] = 0;
      n.forEach(([j, i]) => {
        if (octopusses[j][i] !== 0) {
          octopusses[j][i] += 1;
          if (octopusses[j][i] > 9) {
            flashCount += flash(i, j, octopusses);
          }
        }
      });
      return flashCount + 1;
    }

    function flashAll(octopusses: number[][]): number {
      let flashCount = 0;
      for (let y = 0; y < octopusses.length; y++) {
        for (let x = 0; x < octopusses[y].length; x++) {
          if (octopusses[y][x] > 9) {
            flashCount += flash(x, y, octopusses);
          }
        }
      }
      return flashCount;
    }

    let octopusses = input
      .split("\n")
      .map((line) => line.split("").map((cell) => +cell));
    for (let step = 1; step <= 100; step++) {
      increase(octopusses);
      flashCount += flashAll(octopusses);
    }
    return flashCount;
  }

  const sampleInput = `5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`;

  const actual = process(sampleInput);

  const expected = 1656;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }
  console.log("test ok");
  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day11A();
