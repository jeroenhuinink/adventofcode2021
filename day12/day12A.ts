async function day12A() {
  type Graph = Record<string, string[]>;
  function toEnd(graph: Graph, node: string, path: string[]): string[][] {
    if (node === "end") {
      return [[...path, node]];
    }
    if (node === node.toLowerCase() && path.includes(node)) {
      return [];
    }
    return graph[node].flatMap((to) => toEnd(graph, to, [...path, node]));
  }

  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    const paths = lines.map((line) => line.split("-"));
    const graph = paths.reduce<Graph>((graph, [from, to]) => {
      function addPath(from: string, to: string) {
        if (!graph[from]?.length) {
          graph[from] = [];
        }
        graph[from].push(to);
      }

      if (from === "end") {
        addPath(to, from);
        return graph;
      }

      addPath(from, to);

      if (from !== "start") {
        addPath(to, from);
      }

      return graph;
    }, {});

    return toEnd(graph, "start", []).length;
  }

  function test(sampleInput: string, expected: number) {
    const actual = process(sampleInput);

    if (actual !== expected) {
      throw `Expected ${expected} got ${actual}`;
    }
  }

  test(
    `start-A
start-b
A-c
A-b
b-d
A-end
b-end`,
    10
  );

  test(
    `dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc`,
    19
  );

  test(
    `fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW`,
    226
  );

  console.log("test ok");

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day12A();
