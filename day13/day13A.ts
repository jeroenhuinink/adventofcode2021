async function day13A() {
  function transpose(paper: string[][]) {
    return paper[0].map((_, index) => paper.map((line) => line[index]));
  }

  function max(values: number[]) {
    return values.reduce((max, current) => (current > max ? current : max), 0);
  }

  function countDots(paper: string[][]): number {
    return paper.reduce(
      (total, line) =>
        total +
        line.reduce((dots, cell) => (cell === "#" ? dots + 1 : dots), 0),
      0
    );
  }

  function mapLine(
    top: string[] | undefined,
    bottom: string[] | undefined
  ): string[] {
    if (!top) {
      return bottom!;
    }
    if (!bottom) {
      return top!;
    }
    return top.map((char, index) =>
      char == "#" || bottom[index] == "#" ? "#" : "."
    );
  }

  function foldUp(paper: string[][], line: number): string[][] {
    const folded: string[][] = [];
    const offset = 2 * line - (paper.length - 1);
    for (let c = offset; c < line; c++) {
      folded.push(mapLine(paper[c], paper[paper.length - 1 - (c - offset)]));
    }
    return folded;
  }

  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    const dots = lines
      .filter((line) => !line.startsWith("fold"))
      .map((line) => line.split(",").map((cell) => +cell));
    const folds = lines
      .filter((line) => line.startsWith("fold"))
      .map((line) => {
        const [_, __, fold] = line.split(" ");
        const [dir, pos] = fold.split("=");
        return [dir, +pos];
      });

    const maxY = max(dots.map(([_, y]) => y));
    const maxX = max(dots.map(([x, _]) => x));
    const paper: string[][] = [];
    for (let j = 0; j <= maxY; j++) {
      paper[j] = [];
      for (let i = 0; i <= maxX; i++) {
        paper[j][i] = ".";
      }
    }
    dots.forEach(([x, y]) => {
      paper[y][x] = "#";
    });

    const [dir, pos] = folds[0];

    if (dir == "y") {
      return countDots(foldUp(paper, +pos));
    } else {
      return countDots(transpose(foldUp(transpose(paper), +pos)));
    }
  }

  function test(sampleInput: string, expected: number) {
    const actual = process(sampleInput);

    if (actual !== expected) {
      throw `Expected ${expected} got ${actual}`;
    }
  }

  test(
    `6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5`,
    17
  );

  console.log("test ok");

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day13A();
