async function day13B() {
  function transpose(paper: string[][]) {
    return paper[0].map((_, index) => paper.map((line) => line[index]));
  }

  function max(values: number[]) {
    return values.reduce((max, current) => (current > max ? current : max), 0);
  }

  function display(paper: string[][]): string {
    return paper.map((line) => line.join("")).join("\n");
  }

  function mapLine(
    top: string[] | undefined,
    bottom: string[] | undefined
  ): string[] {
    if (!top) {
      return bottom!;
    }
    if (!bottom) {
      return top!;
    }
    return top.map((char, index) =>
      char == "#" || bottom[index] == "#" ? "#" : "."
    );
  }

  function foldUp(paper: string[][], line: number): string[][] {
    const folded: string[][] = [];
    const offset = 2 * line - (paper.length - 1);

    const fold = paper.length % 2 ? line + 1 : line;
    for (let c = 0; c < fold; c++) {
      folded.push(mapLine(paper[c], paper[paper.length - 1 - (c - offset)]));
    }
    return folded;
  }

  function process(input: string): void {
    const lines = input.split("\n").filter((line) => !!line);
    const dots = lines
      .filter((line) => !line.startsWith("fold"))
      .map((line) => line.split(",").map((cell) => +cell));
    const folds = lines
      .filter((line) => line.startsWith("fold"))
      .map((line) => {
        const [_, __, fold] = line.split(" ");
        const [dir, pos] = fold.split("=");
        return [dir, +pos];
      });

    const maxY = max(dots.map(([_, y]) => y));
    const maxX = max(dots.map(([x, _]) => x));
    const paper: string[][] = [];
    for (let j = 0; j <= maxY; j++) {
      paper[j] = [];
      for (let i = 0; i <= maxX; i++) {
        paper[j][i] = ".";
      }
    }

    dots.forEach(([x, y]) => {
      paper[y][x] = "#";
    });

    const result = folds.reduce<string[][]>((result, [dir, pos]) => {
      if (dir == "y") {
        return foldUp(result, +pos);
      } else {
        return transpose(foldUp(transpose(result), +pos));
      }
    }, paper);

    console.log(display(result));
  }

  function test(sampleInput: string) {
    process(sampleInput);
  }

  test(
    `6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5`
  );

  console.log("test ok");

  const input = await Deno.readTextFile("./input.txt");

  process(input);
}

day13B();
