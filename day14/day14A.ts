async function day14A() {
  function min(values: number[]) {
    return values.reduce(
      (min, current) => (current < min ? current : min),
      Number.MAX_VALUE
    );
  }

  function max(values: number[]) {
    return values.reduce((max, current) => (current > max ? current : max), 0);
  }

  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    const template = lines[0];

    const insertions = lines
      .slice(1)
      .map((line) => line.split(" -> "))
      .reduce<Record<string, string[]>>((accu, [match, insert]) => {
        const [first, second] = match.split("");
        return { [match]: [first + insert, insert + second], ...accu };
      }, {});

    let pairs: Record<string, number> = {};
    for (let i = 0; i < template.length - 1; i++) {
      pairs[template.substr(i, 2)] = 1;
    }

    const validPairs = Object.keys(insertions);

    function insert(insertions: Record<string, string[]>): void {
      const newPairs: Record<string, number> = {};
      Object.keys(pairs).forEach((pair) => {
        insertions[pair].forEach((target) => {
          if (validPairs.includes(target)) {
            if (!newPairs[target]) {
              newPairs[target] = 0;
            }
            newPairs[target] += pairs[pair];
          }
        });
      });
      pairs = newPairs;
    }

    for (let i = 0; i < 10; i++) {
      insert(insertions);
    }

    const counts = Object.values(
      Object.keys(pairs).reduce<Record<string, number>>(
        (accu, key) => {
          const count = pairs[key];
          const char = key.substr(1, 1);
          accu[char] = accu[char] ? accu[char] + count : count;
          return accu;
        },
        { [template.substr(0, 1)]: 1 }
      )
    );
    return max(counts) - min(counts);
  }

  function test(sampleInput: string, expected: number) {
    const actual = process(sampleInput);

    if (actual !== expected) {
      throw `Expected ${expected} got ${actual}`;
    }
  }

  test(
    `NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C`,
    1588
  );

  console.log("test ok");

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day14A();
