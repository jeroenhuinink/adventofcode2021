async function day2B() {
  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);

    function parseLine(line: string) {
      const [direction, countStr] = line.split(" ");

      if (direction === "forward") {
        return { x: +countStr };
      }
      if (direction === "up") {
        return { y: -countStr };
      }
      if (direction === "down") {
        return { y: +countStr };
      }
      throw `unknown direction {${direction}}`;
    }

    const steps = lines.map(parseLine);
    const { x, y } = steps.reduce(
      (accu, step) => ({
        x: accu.x + (step?.x ?? 0),
        y: accu.y + (step?.x ?? 0) * accu.aim,
        aim: accu.aim + (step?.y ?? 0),
      }),
      { x: 0, y: 0, aim: 0 }
    );
    return x * y;
  }

  const sampleInput = `
forward 5
down 5
forward 8
up 3
down 8
forward 2`;

  const actual = process(sampleInput);
  const expected = 900;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }
  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day2B();
