async function day3A() {
  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);

    function countBits(line: string): number[] {
      const bits = line.split("");
      return bits.map((bit) => (bit == "1" ? 1 : -1));
    }

    const bitCounts = lines
      .map(countBits)
      .reduce((accu, bitCount) => bitCount.map((v, i) => accu[i] + v));
    const gammaRate = parseInt(
      bitCounts.map((c) => (c > 0 ? 1 : 0)).join(""),
      2
    );
    const epsilonRate = parseInt(
      bitCounts.map((c) => (c < 0 ? 1 : 0)).join(""),
      2
    );

    return gammaRate * epsilonRate;
  }

  const sampleInput = `
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010`;

  const actual = process(sampleInput);
  const expected = 198;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }
  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day3A();
