async function day3B() {
  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);

    function oxygen(lines: string[], position: number): string[] {
      return filter("1", "0")(lines, position);
    }

    function co2(lines: string[], position: number): string[] {
      return filter("0", "1")(lines, position);
    }

    function filter(first: string, second: string) {
      return (lines: string[], position: number) => {
        const count = lines
          .map((line) => line.substr(position, 1))
          .reduce((accu, val) => (val === "1" ? accu + 1 : accu - 1), 0);
        return lines.filter((line) =>
          count >= 0
            ? line.substr(position, 1) === first
            : line.substr(position, 1) === second
        );
      };
    }

    function findValue(
      lines: string[],
      position: number,
      valueFilter: (lines: string[], position: number) => string[]
    ): string {
      if (lines.length == 1) {
        return lines[0];
      }
      return findValue(valueFilter(lines, position), position + 1, valueFilter);
    }

    const oxygenGeneratorRating = parseInt(findValue(lines, 0, oxygen), 2);

    const co2ScrubberRating = parseInt(findValue(lines, 0, co2), 2);

    return oxygenGeneratorRating * co2ScrubberRating;
  }

  const sampleInput = `
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010`;

  const actual = process(sampleInput);
  const expected = 230;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }
  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day3B();
