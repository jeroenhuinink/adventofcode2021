async function day4A() {
  function checkWin(card: number[][]): boolean {
    const rowWin = card.some((line) => line.every((cell) => cell == -1));
    
    const colWin = [0, 1, 2, 3, 4].some((col) =>
      card.every((line) => line[col] == -1)
    );
    
    return rowWin || colWin;
  }

  function playRound(
    cards: number[][][],
    draw: number[],
    round: number
  ): number[][][] {
    return cards.map((card) =>
      card.map((row) =>
        row.map((cell) => {
          if (cell == draw[round - 1]) {
            return -1;
          } else {
            return cell;
          }
        })
      )
    );
  }

  function process(input: string): number {
    const lines = input.split("\n");

    const draw = lines[0].split(",").map((n) => +n);
    
    let cards: number[][][] = [];
    for (let i = 1; i < lines.length; i++) {
      if (!lines[i]) {
        cards.push([]);
      } else {
        cards[cards.length - 1].push(
          lines[i]
            .trim()
            .split(/\s+/)
            .map((line) => +line.trim())
        );
      }
    }
    cards = cards.filter((card) => card.length !== 0);
    
    let winner = -1;
    let round = 0;
    do {
      round++;

      if (round > draw.length) {
        throw "no winner";
      }
      cards = playRound(cards, draw, round);
      winner = cards.findIndex((card) => checkWin(card));
    } while (winner == -1);
    
    const sumRemaining = cards[winner].reduce(
      (cardTotal, row) =>
        row.reduce(
          (lineTotal, cell) => (cell > -1 ? lineTotal + cell : lineTotal),
          0
        ) + cardTotal,
      0
    );
    return sumRemaining * draw[round - 1];
  }

  const sampleInput = `7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
`;

  const actual = process(sampleInput);
  const expected = 4512;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day4A();
