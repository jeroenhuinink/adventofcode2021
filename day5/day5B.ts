async function day5B() {
  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);

    const segments = lines
      .map((line) => line.split(" -> "))
      .map(([start, end]) => [
        start.split(",").map((n) => +n),
        end.split(",").map((n) => +n),
      ]);
    const size = segments.reduce((size, [start, end]) => {
      let newSize = size;
      if (start[0] > newSize) {
        newSize = start[0];
      }
      if (start[1] > newSize) {
        newSize = start[1];
      }
      if (end[0] > newSize) {
        newSize = end[0];
      }
      if (end[1] > newSize) {
        newSize = end[1];
      }
      return newSize;
    }, 0);

    const matrix: number[][] = [];

    for (let i = 0; i <= size; i++) {
      matrix[i] = [];
      for (let j = 0; j <= size; j++) {
        matrix[i][j] = 0;
      }
    }

    function markCells(
      start: number[],
      end: number[],
      fixedCoord: number,
      variableCoord: number
    ) {
      if (fixedCoord == -1) {
        const dx = start[0] < end[0] ? 1 : -1;
        const dy = start[1] < end[1] ? 1 : -1;
        for (let i = 0; i <= (end[0] - start[0]) * dx; i++) {
          matrix[start[1] + i * dy][start[0] + i * dx] += 1;
        }
      } else {
        for (
          let i =
            start[variableCoord] < end[variableCoord]
              ? start[variableCoord]
              : end[variableCoord];
          start[variableCoord] < end[variableCoord]
            ? i <= end[variableCoord]
            : i <= start[variableCoord];
          i++
        ) {
          if (fixedCoord == 1) {
            matrix[start[fixedCoord]][i] = matrix[start[fixedCoord]][i] + 1;
          } else {
            matrix[i][start[fixedCoord]] = matrix[i][start[fixedCoord]] + 1;
          }
        }
      }
    }

    segments.forEach(([start, end]) => {
      if (start[0] === end[0]) {
        markCells(start, end, 0, 1);
      } else if (start[1] === end[1]) {
        markCells(start, end, 1, 0);
      } else {
        markCells(start, end, -1, -1);
      }
    });

    return matrix.reduce(
      (total, line) =>
        total +
        line.reduce(
          (lineCount, cell) => (cell > 1 ? lineCount + 1 : lineCount),
          0
        ),
      0
    );
  }

  const sampleInput = `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`;

  const actual = process(sampleInput);
  const expected = 12;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day5B();
