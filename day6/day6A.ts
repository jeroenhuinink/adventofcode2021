async function day6A() {
  function process(input: string): number {
    const fishes = input.split(",").map((fish) => +fish.trim());

    for (let day = 0; day < 80; day++) {
      const n = fishes.length;
      for (let j = 0; j < n; j++) {
        if (fishes[j] == 0) {
          fishes[j] = 6;
          fishes.push(8);
        } else {
          fishes[j]--;
        }
      }
    }

    return fishes.length;
  }

  const sampleInput = `3,4,3,1,2`;

  const actual = process(sampleInput);
  const expected = 5934;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day6A();
