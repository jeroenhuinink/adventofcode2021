async function day6B() {
  function process(input: string): number {
    const fishes = input.split(",").map((fish) => +fish.trim());

    function calculateNextGeneration(genX: number[]): number[] {
      const millenials = [0, 0, 0, 0, 0, 0, 0, 0];
      millenials[8] = genX[0];
      millenials[7] = genX[8];
      millenials[6] = genX[7] + genX[0];
      millenials[5] = genX[6];
      millenials[4] = genX[5];
      millenials[3] = genX[4];
      millenials[2] = genX[3];
      millenials[1] = genX[2];
      millenials[0] = genX[1];
      return millenials;
    }
    let generations = [0, 0, 0, 0, 0, 0, 0, 0, 0];

    fishes.forEach((fish) => generations[fish]++);
    for (let day = 0; day < 256; day++) {
      generations = calculateNextGeneration(generations);
    }
    return generations.reduce((t, v) => t + v, 0);
  }

  const sampleInput = `3,4,3,1,2`;

  const actual = process(sampleInput);
  const expected = 26984457539;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day6B();
