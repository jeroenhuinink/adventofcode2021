async function day7B() {
  function sumOfIntegers(n: number) {
    return (n * (n + 1)) / 2;
  }
  function min(values: number[]) {
    return values.reduce(
      (min, current) => (current < min ? current : min),
      Number.MAX_VALUE
    );
  }

  function max(values: number[]) {
    return values.reduce((max, current) => (current > max ? current : max), 0);
  }

  function calcFuel(crabs: number[], target: number) {
    return crabs.reduce(
      (total, crab) => sumOfIntegers(Math.abs(crab - target)) + total,
      0
    );
  }

  function process(input: string): number {
    const crabs = input.split(",").map((crab) => +crab.trim());
    const costs = [...Array(max(crabs)).keys()].map((target) =>
      calcFuel(crabs, target)
    );
    return min(costs);
  }

  const sampleInput = `16,1,2,0,4,2,7,1,2,14`;

  const actual = process(sampleInput);
  const expected = 168;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day7B();
