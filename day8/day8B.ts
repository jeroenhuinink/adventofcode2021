async function day8B() {
  const display = [
    "abcef", //0
    "cf", //1
    "acdeg", //2
    "acdfg", //3
    "bcdf", //4
    "abdfg", //5
    "abdefg", //6
    "acf", //7
    "abcdefg", //8
    "abcdfg", //9
  ];

  function process(input: string): number {
    const lines = input
      .split("\n")
      .filter((line) => !!line)
      .map((line) => line.split(" | "));

    const exclude = (a: string, b: string) => {
      let c = a;
      b.split("").forEach((char) => (c = c.replace(char, "")));
      return c;
    };

    const alphasort = (s: string) => {
      return s.split("").sort().join("");
    };
    const inputs = lines.map(([signals]) => signals);
    const signals = inputs.map((output) => output.split(" "));
    const outputs = lines
      .map(([_, outputs]) => outputs.split(" "))
      .map((digits) => digits.map((n) => alphasort(n)));

    const lineMappings = signals.map((lineSignal) => {
      const one = lineSignal.find((digit) => digit.length === 2) ?? "";
      const four = lineSignal.find((digit) => digit.length === 4) ?? "";
      const seven = lineSignal.find((digit) => digit.length === 3) ?? "";
      const eight = lineSignal.find((digit) => digit.length === 7) ?? "";

      const zero =
        lineSignal.find(
          (digit) =>
            digit.length === 6 &&
            exclude(four, digit).length === 1 &&
            exclude(one, digit).length === 0
        ) ?? "";

      const six =
        lineSignal.find(
          (digit) =>
            digit.length === 6 &&
            exclude(four, digit).length === 1 &&
            exclude(one, digit).length === 1
        ) ?? "";

      const nine =
        lineSignal.find((digit) => {
          return (
            digit.length === 6 &&
            exclude(four, digit).length === 0 &&
            exclude(one, digit).length === 0
          );
        }) ?? "";

      const three =
        lineSignal.find(
          (digit) => digit.length === 5 && exclude(one, digit).length === 0
        ) ?? "";

      const two =
        lineSignal.find(
          (digit) =>
            digit.length === 5 &&
            exclude(one, digit).length === 1 &&
            exclude(digit, nine).length === 1
        ) ?? "";

      const five =
        lineSignal.find(
          (digit) =>
            digit.length === 5 &&
            exclude(one, digit).length === 1 &&
            exclude(digit, nine).length === 0
        ) ?? "";

      return [zero, one, two, three, four, five, six, seven, eight, nine]
        .map((n) => alphasort(n))
        .reduce<Record<string, string>>((a, s, i) => {
          a[s] = i.toString();
          return a;
        }, {});
    });

    let total = 0;
    for (let i = 0; i < outputs.length; i++) {
      const output = outputs[i];
      const mapping = lineMappings[i];
      const value = +output.map((digit) => mapping[digit]).join("");
      total += value;
    }
    return total;
  }

  const sampleInput = `be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce`;

  const actual = process(sampleInput);
  const expected = 61229;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day8B();

/*

gf
gaef


*/
