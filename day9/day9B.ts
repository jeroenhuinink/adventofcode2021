interface Point {
  x: number;
  y: number;
}

async function day9B() {
  function neighbours({ x, y }: Point, map: number[][]): Point[] {
    const maxY = map.length - 1;
    const maxX = map[0].length - 1;
    const neighbours: Point[] = [];

    if (x > 0) {
      neighbours.push({ x: x - 1, y: y });
    }
    if (x < maxX) {
      neighbours.push({ x: x + 1, y: y });
    }
    if (y > 0) {
      neighbours.push({ x: x, y: y - 1 });
    }
    if (y < maxY) {
      neighbours.push({ x: x, y: y + 1 });
    }

    return neighbours;
  }

  function lowest(point: Point, map: number[][]): boolean {
    return neighbours(point, map).every((neighbour) => {
      return map[neighbour.y][neighbour.x] > map[point.y][point.x];
    });
  }

  function findBasin(point: Point, basin: Point[], map: number[][]): Point[] {
    //already in the basin.
    if (basin.find((b) => b.x == point.x && b.y == point.y)) {
      return basin;
    }
    const potentials = neighbours(point, map).filter(
      (neighbour) => map[neighbour.y][neighbour.x] < 9
    );
    let newBasin = [point, ...basin];
    for (const potential of potentials) {
      newBasin = [...newBasin];
      newBasin = findBasin(potential, newBasin, map);
    }
    return newBasin;
  }

  function process(input: string): number {
    const lines = input.split("\n").filter((line) => !!line);
    const map = lines.map((line) => line.split("").map((v) => +v));
    const lowPoints: Point[] = [];
    for (let y = 0; y < map.length; y++) {
      for (let x = 0; x < map[y].length; x++) {
        if (lowest({ x, y }, map)) {
          lowPoints.push({ x, y });
        }
      }
    }
    const basinSizes = lowPoints
      .map((point) => findBasin(point, [], map))
      .map((basin) => basin.length);
    basinSizes.sort((a, b) => (a < b ? 1 : -1));

    return basinSizes[0] * basinSizes[1] * basinSizes[2];
  }

  const sampleInput = `2199943210
3987894921
9856789892
8767896789
9899965678`;

  const actual = process(sampleInput);
  const expected = 1134;
  if (actual !== expected) {
    throw `Expected ${expected} got ${actual}`;
  }

  const input = await Deno.readTextFile("./input.txt");

  console.log(process(input));
}

day9B();
